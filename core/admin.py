# coding: utf-8

from daterange_filter.filter import DateRangeFilter
from django.contrib import admin
from django.urls import reverse
from django.shortcuts import redirect
from django.conf.urls import url
from django.contrib.admin import BooleanFieldListFilter
from import_export.admin import ExportMixin
from .forms import OrderForm
from .models import Product, OrderItem, Order, Customer


class OrderItemInlineAdmin(admin.TabularInline):
    model = OrderItem
    extra = 1
    fields = ('product', 'get_price', 'amount')
    readonly_fields = ('get_price',)

    def get_price(self, instance):
        return instance.product.price
    get_price.short_description = 'Preço'


class OrderAdmin(ExportMixin, admin.ModelAdmin):
    form = OrderForm
    list_display = ('customer', 'created_at', 'get_total', 'is_paid')
    inlines = (OrderItemInlineAdmin,)
    search_fields = ('customer__name', 'seller__username')
    list_filter = ('customer', 'seller', ('created_at', DateRangeFilter))
    # list_filter = ('customer', 'seller', 'created_at',)
    # actions = ('make_payment', 'undo_payment')
    exclude = ('paid_at',)

    def get_changeform_initial_data(self, request):
        initial_data = super(OrderAdmin, self).get_changeform_initial_data(request)
        initial_data['seller'] = request.user
        return initial_data

    def is_paid(self, instance):
        return 'Sim' if instance.is_paid() else 'Não'
    is_paid.short_description = 'Pago?'

#    def save_model(self, request, obj, form, change):
#        super(OrderAdmin, self).save_model(self, request, obj, form, change)


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'stock')
    exclude = ('slug',)
    search_fields = ('name', 'slug')
    list_filter = ('name',)


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('name', 'genre', 'balance')
    search_fields = ('name',)
    list_filter = ('name', 'genre')


admin.site.register(Product, ProductAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderItem)
admin.site.register(Customer, CustomerAdmin)
