# coding: utf-8

from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from .models import OrderItem


# @receiver(post_save, sender=OrderItem)
# def withdraw_stock(sender, **kwargs):
#     instance = kwargs.get('instance', None)
#     print(instance.withdraw_stock)
#     if instance.withdraw_stock:
#         print(instance.orderitem_set.all())
#         for item in instance.orderitem_set.all():
#             item.product.stock -= item.amount
#             item.product.save()
