# coding: utf-8

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView

from core.forms import ProductForm, CustomerForm, OrderForm, OrderProductFormset
from core.models import Product, Customer, Order


LoginRequiredMixin.login_url = reverse_lazy('login')


class ProductList(LoginRequiredMixin, ListView):
    model = Product
    template_name = 'core/product/list.html'
product_list = ProductList.as_view()


class ProductCreate(LoginRequiredMixin, CreateView):
    model = Product
    form_class = ProductForm
    template_name = 'core/product/create.html'
    success_url = reverse_lazy('core:product_list')
product_create = ProductCreate.as_view()


class ProductDetail(LoginRequiredMixin, DetailView):
    model = Product
    template_name = 'core/product/detail.html'
product_detail = ProductDetail.as_view()


class ProductUpdate(LoginRequiredMixin, UpdateView):
    model = Product
    form_class = ProductForm
    template_name = 'core/product/update.html'
    success_url = reverse_lazy('core:product_list')
product_update = ProductUpdate.as_view()


class ProductDelete(LoginRequiredMixin, DeleteView):
    model = Product
    success_url = reverse_lazy('core:product_list')
product_delete = ProductDelete.as_view()


class CustomerList(LoginRequiredMixin, ListView):
    model = Customer
    template_name = 'core/customer/list.html'
customer_list = CustomerList.as_view()


class CustomerCreate(LoginRequiredMixin, CreateView):
    model = Customer
    form_class = CustomerForm
    template_name = 'core/customer/create.html'
    success_url = reverse_lazy('core:customer_list')
customer_create = CustomerCreate.as_view()


class CustomerDetail(LoginRequiredMixin, DetailView):
    model = Customer
    slug_field = 'pk'
    template_name = 'core/customer/detail.html'
customer_detail = CustomerDetail.as_view()


class CustomerUpdate(LoginRequiredMixin, UpdateView):
    model = Customer
    form_class = CustomerForm
    template_name = 'core/customer/update.html'
    success_url = reverse_lazy('core:customer_list')
customer_update = CustomerUpdate.as_view()


class CustomerDelete(LoginRequiredMixin, DeleteView):
    model = Customer
    success_url = reverse_lazy('core:customer_list')
customer_delete = CustomerDelete.as_view()


class OrderList(LoginRequiredMixin, ListView):
    model = Order
    template_name = 'core/order/list.html'
order_list = OrderList.as_view()


class OrderCreate(LoginRequiredMixin, CreateView):
    model = Order
    form_class = OrderForm
    formset_classes = {'product': OrderProductFormset}
    template_name = 'core/order/create.html'
    success_url = reverse_lazy('core:order_list')

    def get(self, request, *args, **kwargs):
        self.formsets = self.get_formsets(request)
        return super(OrderCreate, self).get(request, *args, **kwargs)

    def get_context_data(self):
        ctx = super(OrderCreate, self).get_context_data()
        ctx['formsets'] = self.formsets
        return ctx

    def get_formsets(self, request):
        formsets = {}
        for name, formset_class in self.formset_classes.items():
            formsets[name] = formset_class(request.POST or None)
        return formsets
order_create = OrderCreate.as_view()


class OrderDetail(LoginRequiredMixin, DetailView):
    model = Order
    slug_field = 'pk'
    template_name = 'core/order/detail.html'
order_detail = OrderDetail.as_view()


class OrderUpdate(LoginRequiredMixin, UpdateView):
    model = Order
    form_class = OrderForm
    template_name = 'core/order/update.html'
    success_url = reverse_lazy('core:order_list')
order_update = OrderUpdate.as_view()


class OrderDelete(LoginRequiredMixin, DeleteView):
    model = Order
    success_url = reverse_lazy('core:order_list')
order_delete = OrderDelete.as_view()
