# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-28 18:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_auto_20170528_1500'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='note',
            field=models.TextField(blank=True, null=True, verbose_name='Observação'),
        ),
    ]
