# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-28 14:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_auto_20170527_2136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='discount',
            field=models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=4, null=True, verbose_name='Desconto'),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='amount',
            field=models.PositiveIntegerField(default=1, verbose_name='Quantidade'),
        ),
    ]
