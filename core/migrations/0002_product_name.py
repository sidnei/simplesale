# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-23 02:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='name',
            field=models.CharField(default='', max_length=80),
            preserve_default=False,
        ),
    ]
