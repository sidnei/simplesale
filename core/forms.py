# coding: utf-8

from django import forms
from django_select2.forms import ModelSelect2Widget
from django.utils.timezone import datetime

from .models import Product, Customer, Order, OrderItem


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        exclude = ('slug',)


class OrderProductForm(forms.ModelForm):
    product = forms.ModelChoiceField(queryset=OrderItem.objects.all())
    price = forms.DecimalField()
    amount = forms.IntegerField()
    discount = forms.DecimalField()
    total = forms.DecimalField(
        forms.NumberInput(
            attrs={'readonly': True}
        )
    )

    class Meta:
        model = Product
        exclude = ('slug', 'name', 'stock',)
        widgets = {
            'product': ModelSelect2Widget(queryset=OrderItem.objects.all()),
        }



class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = '__all__'


class OrderForm(forms.ModelForm):
    paid = forms.BooleanField(required=False, label=u'Pago?')

    class Meta:
        model = Order
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        if self.instance and self.instance.pk:
            self.fields['paid'].initial = bool(self.instance.paid_at)

    def clean_paid(self):
        paid = self.cleaned_data['paid']
        if paid:
            self.instance.paid_at = datetime.now()
        else:
            self.instance.paid_at = None

        return self.cleaned_data['paid']


class OrderItemForm(forms.ModelForm):
    class Meta:
        model = OrderItem
        fields = '__all__'
        widgets = {
            'product': ModelSelect2Widget(queryset=OrderItem.objects.all()),
        }


OrderProductFormset = forms.formset_factory(OrderProductForm)
