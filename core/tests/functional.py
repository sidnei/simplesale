from django.test import TestCase
from core.models import Product


class Products(TestCase):
    def test_create_product(self):
        product_name = 'New product'

        response = self.client.post('/products/add/', data={'name': product_name})

        self.assertRedirects(response, '/products/')
        self.assertGreater(len(Product.objects.filter(name=product_name)), 0)


    def test_page_should_list_products(self):
        # When user access "/products" it should list the products
        product1 = Product.objects.create(name='Product 1')
        product2 = Product.objects.create(name='Product 2')

        response = self.client.get('/products/')

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'core/list.html')
        self.assertContains(response, product1.name)
        self.assertContains(response, product2.name)