# coding: utf-8

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.text import slugify
from django.utils.timezone import datetime


User = get_user_model()


class Product(models.Model):
    name = models.CharField('Nome', max_length=80)
    slug = models.SlugField('Slug', unique=True)
    price = models.DecimalField(u'Preço', max_digits=4, decimal_places=2, null=True, blank=True)
    stock = models.PositiveIntegerField('Estoque', null=True, blank=True)

    class Meta:
        verbose_name = 'Produto'
        verbose_name_plural = 'Produtos'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.pk:
            self._generate_slug()
        super(Product, self).save(*args, **kwargs)

    def _generate_slug(self):
        slug = self.slug or slugify(self.name)
        self.slug = slug
        i = 0
        while True:
            try:
                self.__class__.objects.get(slug=self.slug)
                i += 1
                self.slug = u'{}-{}'.format(slug, i)
            except self.__class__.DoesNotExist:
                break


class Customer(models.Model):
    _DEFAULT_GENRE_CHOICE = ('N', u'Não declarado')
    _GENRE_CHOICES = (
        _DEFAULT_GENRE_CHOICE,
        ('M', 'Masculino'),
        ('F', 'Feminino'),
    )

    name = models.CharField('Nome', max_length=80)
    genre = models.CharField('Sexo', max_length=2, choices=_GENRE_CHOICES, default=_DEFAULT_GENRE_CHOICE[0])
    balance = models.DecimalField('Saldo', max_digits=4, decimal_places=2, null=True, blank=True, default=0)
    origin = models.CharField(
        'Origem', max_length=40, null=True, blank=True,
        help_text='De onde conhece o cliente(e.g. Bradesco, Secretaria)'
    )

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'

    def __str__(self):
        return self.name


class Order(models.Model):
    customer = models.ForeignKey(Customer, verbose_name='Cliente')
    seller = models.ForeignKey(User, verbose_name='Vendedor')
    created_at = models.DateTimeField('Data do pedido', default=datetime.now)
    paid_at = models.DateTimeField("Data do pagamento", null=True, blank=True)
    delivery_at = models.DateTimeField("Data de entrega", null=True, blank=True, default=datetime.now)
    discount = models.DecimalField('Desconto', max_digits=4, decimal_places=2, null=True, blank=True, default=0)
    note = models.TextField('Observação', null=True, blank=True)

    class Meta:
        verbose_name = 'Pedido'
        verbose_name_plural = 'Pedidos'

    def __str__(self):
        return str(self.pk)

    def get_total(self):
        total = 0
        for item in self.orderitem_set.all():
            total += item.get_total()
        if self.discount:
            total -= self.discount
        return total
    get_total.short_description = 'Total'

    def is_paid(self):
        return bool(self.pk and self.paid_at)
    is_paid.short_description = u'Pago?'

    def save(self, *args, **kwargs):
        withdraw_stock = not self.pk
        super(Order, self).save(*args, **kwargs)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, verbose_name='Pedido')
    product = models.ForeignKey(Product, verbose_name='Produto')
    amount = models.PositiveIntegerField('Quantidade', default=1)

    class Meta:
        verbose_name = 'Item do pedido'
        verbose_name_plural = 'Itens dos pedidos'

    def __str__(self):
        return self.product.name

    def get_total(self):
        return self.amount * self.product.price

    def save(self, *args, **kwargs):
        withdraw_stock = not self.pk
        super(OrderItem, self).save(*args, **kwargs)
        print (withdraw_stock)
