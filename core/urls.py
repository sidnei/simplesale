# coding: utf-8

from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^products/$', views.product_list, name='product_list'),
    url(r'^products/add/$', views.product_create, name='product_create'),
    url(r'^products/(?P<slug>[\w-]+)/$', views.product_detail, name='product_detail'),
    url(r'^products/(?P<slug>[\w-]+)/change/$', views.product_update, name='product_update'),
    url(r'^products/(?P<slug>[\w-]+)/delete/$', views.product_delete, name='product_delete'),

    url(r'^customers/$', views.customer_list, name='customer_list'),
    url(r'^customers/add/$', views.customer_create, name='customer_create'),
    url(r'^customers/(?P<pk>\d+)/$', views.customer_detail, name='customer_detail'),
    url(r'^customers/(?P<pk>\d+)/change/$', views.customer_update, name='customer_update'),
    url(r'^customers/(?P<pk>\d+)/delete/$', views.customer_delete, name='customer_delete'),

    url(r'^orders/$', views.order_list, name='order_list'),
    url(r'^orders/add/$', views.order_create, name='order_create'),
    url(r'^orders/(?P<pk>\d+)/$', views.order_detail, name='order_detail'),
    url(r'^orders/(?P<pk>\d+)/change/$', views.order_update, name='order_update'),
    url(r'^orders/(?P<pk>\d+)/delete/$', views.order_delete, name='order_delete'),
]
