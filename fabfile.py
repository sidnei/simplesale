# -*- coding: utf-8 -*-
from __future__ import with_statement
import os

from fabric.api import cd, env, local, prefix, run, sudo, task
from fabric.colors import blue, green, red
from contextlib import contextmanager as _contextmanager

VIRTUALENV_NAME = os.environ.get('VENV', 'simplesale')
env.hosts = ['simplesale.sidnei.me']
env.user = 'sidnei'
env.directory = '/home/sidnei/repos/simplesale'
env.activate = 'source $HOME/.venvs/%s/bin/activate' % VIRTUALENV_NAME


def msg(message, msg_type="run"):
    if "run" == msg_type:
        output = blue("Running: ") + message
    elif "success" == msg_type:
        output = green("Success: ") + message
    elif "error" == msg_type:
        output = red("Error: ") + message
    if env.host_string:
        output = '[' + env.host_string + '] ' + output
    print(output)


def git(command, remote=False):
    git_command = "git %s" % command
    msg(git_command)
    if remote:
        return run(git_command)
    return local(git_command, capture=True)


def requirements_changed(remote=False):
    result = git("diff origin/develop -- requirements.txt", remote)
    return bool(len(result))


@task
def reqs():
    print(requirements_changed())


@_contextmanager
def virtualenv():
    with prefix(env.activate):
        yield


@task
def restart():
    sudo('supervisorctl restart simplesale')
    sudo('service nginx reload')


@task
def update():
    with virtualenv():
        new_packages = requirements_changed()
        local('git pull origin master')
        if new_packages:
            local('pip install -r requirements.txt')
        local('supervisorctl restart simplesale')
        local('sudo service nginx reload')


@task
def deploy():
    with cd(env.directory):
        with prefix(env.activate):
            new_packages = requirements_changed(True)
            run('git pull origin master')
            if new_packages:
                run('pip install -r requirements.txt')
            run('python manage.py migrate')
        restart()


@task
def soft_deploy():
    with cd(env.directory):
        with prefix(env.activate):
            new_packages = requirements_changed(True)
            run('git pull origin master')
            # if new_packages:
            #     run('pip install -r requirements.txt')
            # run('python manage.py migrate')
    sudo('supervisorctl restart simplesale')


@task
def collect_static():
    with cd(env.directory):
        with prefix(env.activate):
            run('python manage.py collectstatic --noinput')
